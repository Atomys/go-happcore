module gitlab.com/Atomys/go-happcore

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.3
	github.com/rs/zerolog v1.23.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.13
)
