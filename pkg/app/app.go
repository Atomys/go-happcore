package app

import (
	"context"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog"
	"gorm.io/gorm"
)

type App struct {
	opts appOptions
}

type appOptions struct {
	ctx         context.Context
	logger      *zerolog.Logger
	gormDB      *gorm.DB
	redisClient *redis.Client
}

var defaultAppOptions = appOptions{
	ctx: context.TODO(),
}

func NewApp(opt ...appOption) (*App, error) {
	opts := defaultAppOptions

	for _, o := range opt {
		o.apply(&opts)
	}

	var app = &App{
		opts: opts,
	}

	return app, nil
}

// A appOption sets options such as credentials, codec and keepalive parameters, etc.
type appOption interface {
	apply(*appOptions)
}

// funcAppOption wraps a function that modifies appOptions into an
// implementation of the AppOption interface.
type funcAppOption struct {
	f func(*appOptions)
}

func (fdo *funcAppOption) apply(do *appOptions) {
	fdo.f(do)
}

func newFuncAppOption(f func(*appOptions)) *funcAppOption {
	return &funcAppOption{
		f: f,
	}
}
