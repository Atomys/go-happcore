package app

import (
	"testing"
)

// EmptyServerOption does not alter the server configuration. It can be embedded
// in another structure to build custom server options.
//
// Experimental
//
// Notice: This type is EXPERIMENTAL and may be changed or removed in a
// later release.
type EmptyServerOption struct{}

func (EmptyServerOption) apply(*appOptions) {}

func TestNewAppWithtoutOptions(t *testing.T) {
	a, _ := NewApp()

	if a.Logger() != nil {
		t.Errorf("AppWithtoutOptions fail. Want nil got %v", a.Logger())
	}
}

func TestNewAppWithEmptyOpt(t *testing.T) {
	e := EmptyServerOption{}
	a, _ := NewApp(e)
	a.Logger()
}
