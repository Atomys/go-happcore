package app

import (
	"context"
)

// WithContext will instanciate a logger for this application
func WithContext(ctx context.Context) appOption {
	return newFuncAppOption(func(o *appOptions) {
		o.ctx = ctx
	})
}

// Context return the context configured with the AppOption `withContext`
// during the newApp call or an empty context
func (a *App) Context() context.Context {
	return a.opts.ctx
}
