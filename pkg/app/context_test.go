package app

import (
	"context"
	"testing"
)

func TestWithContext(t *testing.T) {
	type ctxKey string
	var ck ctxKey = "test"
	a, _ := NewApp(WithContext(context.WithValue(context.TODO(), ck, true)))

	if a.Context().Value(ck).(bool) != true {
		t.Errorf("context isn't pass correctly. Want true got %t", a.Context().Value(ck))
	}
}
