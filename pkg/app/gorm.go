package app

import (
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// WithGorm will instanciate a new gorm Client connection based on
// value given in environnment variable GORM_DATABASE_URI
func WithGorm() appOption {
	client, err := gorm.Open(postgres.Open(os.Getenv("GORM_DATABASE_URI")))
	if err != nil {
		panic(err)
	}

	return newFuncAppOption(func(o *appOptions) {
		o.gormDB = client
	})
}

// GormDB return the gomrm DB object configured with the
// AppOption `WithGorm` during the newApp call
func (a *App) GormDB() *gorm.DB {
	return a.opts.gormDB
}
