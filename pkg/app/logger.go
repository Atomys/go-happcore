package app

import (
	"os"

	"github.com/rs/zerolog"
)

// WithLogger will instanciate a logger for this application
func WithLogger(appName string, level zerolog.Level) appOption {
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).
		Level(level).
		With().
		Timestamp().
		Str("app-name", appName).
		Logger()
	return newFuncAppOption(func(o *appOptions) {
		o.logger = &logger
	})
}

// Logger return the logger configured with the AppOption `withLogger`
// during the newApp call
func (a App) Logger() *zerolog.Logger {
	return a.opts.logger
}
