package app

import (
	"testing"

	"github.com/rs/zerolog"
)

func TestWithLogger(t *testing.T) {
	testName := "why-are-u-running"
	a, _ := NewApp(WithLogger(testName, zerolog.FatalLevel))

	if a.opts.logger.GetLevel() != zerolog.FatalLevel {
		t.Errorf("context isn't pass correctly. Want zerolog.FatalLevel got %v", a.Logger().GetLevel())
	}

	if a.Logger().GetLevel() != zerolog.FatalLevel {
		t.Errorf("context isn't pass correctly. Want zerolog.FatalLevel got %v", a.Logger().GetLevel())
	}
}
