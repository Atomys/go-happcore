package app

import (
	"os"

	"github.com/go-redis/redis/v8"
)

// WithRedis will instanciate a new Redis Client connection based on
// value given in environnment variable REDIS_URI
func WithRedis(opts ...*redis.Options) appOption {
	if os.Getenv("REDIS_URI") != "" && len(opts) == 0 {
		opt, err := redis.ParseURL(os.Getenv("REDIS_URI"))
		if err != nil {
			panic(err)
		}
		opts = []*redis.Options{opt}
	}
	client := redis.NewClient(opts[0])
	return newFuncAppOption(func(o *appOptions) {
		o.redisClient = client
	})
}

// Redis return the redis Client object configured with the
// AppOption `WithRedis` during the newApp call
func (a *App) Redis() *redis.Client {
	return a.opts.redisClient
}
