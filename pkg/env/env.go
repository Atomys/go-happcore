package env

import (
	"os"
)

// Production return true is GO_ENV environment variable is
// set to production
func Production() bool {
	return os.Getenv("GO_ENV") == "production"
}

// Staging return true is GO_ENV environment variable is
// set to staging
func Staging() bool {
	return os.Getenv("GO_ENV") == "staging"
}

// Development return true is GO_ENV environment variable is
// set to development
func Development() bool {
	return os.Getenv("GO_ENV") == "development"
}
