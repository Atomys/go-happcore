package helpersttp

import (
	"encoding/json"
	"net/http"
)

// RespondWithError will write on HTTP Response Writter to respond
// a structured JSON error
func RespondWithError(w http.ResponseWriter, httpCode int, errorMsg string) {
	RespondWithJSON(w, httpCode, map[string]string{"error": errorMsg})
}

// RespondWithJSON will takes the payload and marshal it as JSON before write on
// ResponseWriter and set the Contet-Type header
func RespondWithJSON(w http.ResponseWriter, httpCode int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	w.Write(response)
}
