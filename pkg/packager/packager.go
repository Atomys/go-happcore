package packager

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"io"
	"strings"
	"time"
)

// File represents the file structure and informations
// about file who needs to added on an archive.
type File struct {
	Name    string
	Content []byte
	Mode    int64
	ModTime time.Time
}

// CreateTarGz will take all given files and archive it on a .tar.gz
// archive. The archive will be write on `buf` at the time.
func CreateTarGz(buf *bytes.Buffer, files ...File) error {
	gw := gzip.NewWriter(buf)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()

	// Iterate over files and add them to the tar archive
	for _, file := range files {
		header := new(tar.Header)
		header.Name = file.Name
		header.Size = int64(len(file.Content))
		header.Mode = file.Mode
		header.ModTime = file.ModTime
		// write the header to the tarball archive
		if err := tw.WriteHeader(header); err != nil {
			return err
		}
		// copy the file data to the tarball
		if _, err := io.Copy(tw, strings.NewReader(string(file.Content))); err != nil {
			return err
		}
	}

	return nil
}
