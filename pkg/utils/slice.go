package utils

// Contains return a boolean if element is in slice
func Contains(slice interface{}, element interface{}) bool {
	switch set := slice.(type) {
	case []string:
		for _, a := range set {
			if a == element {
				return true
			}
		}
	case []int:
		for _, a := range set {
			if a == element {
				return true
			}
		}
	}

	return false
}
