package utils

import (
	"testing"
)

func TestToCamel(t *testing.T) {
	testSuits := map[string]string{
		"test_case":            "TestCase",
		"test":                 "Test",
		"TestCase":             "TestCase",
		" test  case ":         "TestCase",
		"":                     "",
		"many_many_words":      "ManyManyWords",
		"AnyKind of_string":    "AnyKindOfString",
		"odd-fix":              "OddFix",
		"numbers2And55with000": "Numbers2And55With000",
	}

	for snake, camel := range testSuits {
		converted := ToCamel(snake)
		if camel != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", camel, converted)
		}
	}
}

func TestToLowerCamel(t *testing.T) {
	testSuits := map[string]string{
		"foo-bar":           "fooBar",
		"TestCase":          "testCase",
		"":                  "",
		"AnyKind of_string": "anyKindOfString",
	}
	for any, lower := range testSuits {
		converted := ToLowerCamel(any)
		if lower != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", lower, converted)
		}
	}
}

func TestToSnake(t *testing.T) {
	testSuits := map[string]string{
		"testCase":             "test_case",
		"TestCase":             "test_case",
		"Test Case":            "test_case",
		" Test Case":           "test_case",
		"Test Case ":           "test_case",
		" Test Case ":          "test_case",
		"test":                 "test",
		"test_case":            "test_case",
		"Test":                 "test",
		"Skip____underscore":   "skip_underscore",
		"":                     "",
		"ManyManyWords":        "many_many_words",
		"manyManyWords":        "many_many_words",
		"AnyKind of_string":    "any_kind_of_string",
		"numbers2and55with000": "numbers_2_and_55_with_000",
		"JSONData":             "json_data",
		"userID":               "user_id",
		"AAAbbb":               "aa_abbb",
	}
	for any, snake := range testSuits {
		converted := ToSnake(any)
		if snake != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", snake, converted)
		}
	}
}

func TestToDelimited(t *testing.T) {
	testSuits := map[string]string{
		"testCase":             "test@case",
		"TestCase":             "test@case",
		"Test Case":            "test@case",
		" Test Case":           "test@case",
		"Test Case ":           "test@case",
		" Test Case ":          "test@case",
		"test":                 "test",
		"test_case":            "test@case",
		"Test":                 "test",
		"":                     "",
		"ManyManyWords":        "many@many@words",
		"manyManyWords":        "many@many@words",
		"AnyKind of_string":    "any@kind@of@string",
		"numbers2and55with000": "numbers@2@and@55@with@000",
		"JSONData":             "json@data",
		"userID":               "user@id",
		"AAAbbb":               "aa@abbb",
		"test-case":            "test@case",
	}
	for any, delimited := range testSuits {
		converted := ToDelimited(any, '@')
		if delimited != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", delimited, converted)
		}
	}
}

func TestToScreamingSnake(t *testing.T) {
	testSuits := map[string]string{
		"testCase": "TEST_CASE",
	}
	for any, screamingSnake := range testSuits {
		converted := ToScreamingSnake(any)
		if screamingSnake != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", screamingSnake, converted)
		}
	}
}

func TestToKebab(t *testing.T) {
	testSuits := map[string]string{
		"testCase": "test-case",
	}
	for any, kebab := range testSuits {
		converted := ToKebab(any)
		if kebab != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", kebab, converted)
		}
	}
}

func TestToScreamingKebab(t *testing.T) {
	testSuits := map[string]string{
		"testCase": "TEST-CASE",
	}
	for any, screamingKebab := range testSuits {
		converted := ToScreamingKebab(any)
		if screamingKebab != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", screamingKebab, converted)
		}
	}
}

func TestToScreamingDelimited(t *testing.T) {
	testSuits := map[string]string{
		"testCase": "TEST.CASE",
	}
	for any, screamingDelimited := range testSuits {
		converted := toScreamingDelimited(any, '.', true)
		if screamingDelimited != converted {
			t.Errorf("Unexpected conversion: expected %s got %s", screamingDelimited, converted)
		}
	}
}
