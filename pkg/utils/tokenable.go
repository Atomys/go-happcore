package utils

import (
	"crypto/rand"
	"math/big"
)

type TokenConfig struct {
	Letter  bool
	Number  bool
	Special bool
}

const (
	letterBytes  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	numberBytes  = "0123456789"
	specialBytes = "@#$%{}[]*-_"
)

func Token(length int, conf ...*TokenConfig) (string, error) {
	var c *TokenConfig
	if len(conf) == 0 {
		c = &TokenConfig{true, true, false}
	} else {
		c = conf[0]
	}

	if length < 1 {
		return "", nil
	}

	// Resolve char used to generate token
	sliceByte := slicebyConf(c)
	// Define the max iterator for find a random char
	// Create a new slice of byte to store random string generated
	maxIterator := big.NewInt(int64(len(sliceByte)))
	b := make([]byte, length)
	for i := range b {
		randomIndex, err := rand.Int(rand.Reader, maxIterator)
		if err != nil {
			return "", err
		}

		b[i] = sliceByte[randomIndex.Int64()]
	}
	return string(b), nil
}

func slicebyConf(c *TokenConfig) []byte {
	var b []byte

	if c.Letter {
		b = append(b, letterBytes...)
	}
	if c.Number {
		b = append(b, numberBytes...)
	}
	if c.Special {
		b = append(b, specialBytes...)
	}

	return b
}
