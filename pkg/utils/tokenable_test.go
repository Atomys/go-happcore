package utils

import (
	"regexp"
	"testing"
)

func TestTokenLength(t *testing.T) {
	tests := []int{-1, 0, 1, 16, 42, 64, 128, 1024}

	for _, length := range tests {
		token, _ := Token(length)
		if len(token) != length && (length < 0 && token != "") {
			t.Errorf("Token hasn't desired length (%d): %s", length, token)
		}
	}
}

func TestTokenConfig(t *testing.T) {
	var token string
	var re *regexp.Regexp

	token, _ = Token(8, &TokenConfig{true, false, false})
	re = regexp.MustCompile(`\W|\d`)
	if m := re.FindStringSubmatch(token); len(m) > 0 {
		t.Errorf("Token generated with only letter mode return unexcepted character: %v", m)
	}

	token, _ = Token(8, &TokenConfig{false, true, false})
	re = regexp.MustCompile(`\D|\W`)
	if m := re.FindStringSubmatch(token); len(m) > 0 {
		t.Errorf("Token generated with only number mode return unexcepted character: %v", m)
	}

	token, _ = Token(8, &TokenConfig{false, false, true})
	re = regexp.MustCompile(`[a-zA-Z]|\d`)
	if m := re.FindStringSubmatch(token); len(m) > 0 {
		t.Errorf("Token generated with only special mode return unexcepted character: %v", m)
	}

}
